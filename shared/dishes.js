export const DISHES =
    [
        {
        id: 0,
        name:'Cotton fine-knit t-shirt',
        image: 'images/uthappizza.png',
        category: 'T-Shirts',
        label:'Hot',
        price:'42.99',
        featured: true,
        description:'Smart collection. Knitted fabric. Cotton fabric. Contrasting band. Rounded neck. Short sleeve. Cable knit finish.'                    
        },
        {
        id: 1,
        name:'Cotton linen-blend knit t-shirt',
        image: 'images/zucchipakoda.png',
        category: 'T-Shirts',
        label:'New',
        price:'16.99',
        featured: false,
        description:'Casual line. Cotton fabric. Fabric with linen. Knitted fabric. Combined design. Rounded neck. Patch pocket on the chest. Short sleeve. Elastic finish.'
        },
        {
        id: 2,
        name:'Cotton linen-blend knit t-shirt',
        image: 'images/vadonut.png',
        category: 'T-Shirts',
        label:'New',
        price:'10.99',
        featured: false,
        description:'Casual line. Cotton fabric. Fabric with linen. Knitted fabric. Combined design. Rounded neck. Patch pocket on the chest. Short sleeve. Elastic finish.'
        },
        {
        id: 3,
        name:'Rolling Stones T-shirt',
        image: 'images/elaicheesecake.png',
        category: 'T-Shirts',
        label:'New',
        price:'20.99',
        featured: false,
        description:'Casual line. Cotton fabric. Rock style. Print on the front. Rounded neck. Short sleeve.'
        }
    ];